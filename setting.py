config = {
    "src_folder": "dev/root",
    "success_raw_folder": "success_raw",  # store raw csv succesfully processed
    "output_folder": "output",  # store csvs for uploading to SFTP
    "failure_folder": "failure",
    "dry_run": True,  # whether to move raw file locally
}
