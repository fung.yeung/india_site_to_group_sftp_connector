from util import get_logger


logger = get_logger(logger_name=__name__)

from util import NagpurUploader


def main(config: dict):

    logger.info("process started")

    nu = NagpurUploader(config)
    logger.info(f"initiated - config: {config}")

    logger.info(f"processing raw CSV to SFTP format")
    nu.raw_to_sftp_format()
    logger.info(f"process end for raw CSV to SFTP format")

    logger.info(f"uploading SFTP CSV to SFTP server")
    nu.to_sftp()
    logger.info(f"completed upload SFTP CSV to SFTP server")

    logger.info(f"cleaning local files")
    nu.clean_up_files()
    logger.info(f"completed clean up local files")

    logger.info("process completed")


if __name__ == "__main__":
    from setting import config

    main(config=config)
