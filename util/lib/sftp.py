import paramiko


class SFTP:
    """
    Easy access for SFTP server, usage:

    doc:
    https://docs.paramiko.org/en/stable/api/sftp.html#paramiko.sftp_client.SFTPClient"""

    def __init__(self, credentials: dict[str, str]):
        """

        Args:
            credentials (dict[str, str]): expects host, username, password, port(optional)
        """
        self.credentials = credentials

    def __enter__(self) -> paramiko.SFTPClient:
        # ttysetattr etc goes here before opening and returning the file object
        credentials = self.credentials
        transport = paramiko.Transport(
            sock=(credentials["host"], credentials.get("port", 22))
        )
        transport.connect(
            username=credentials["username"], password=credentials["password"]
        )
        self.sftp = paramiko.SFTPClient.from_transport(transport)
        return self.sftp

    def __exit__(self, type, value, traceback):
        self.sftp.close()

    def get(self, remotepath: str, localpath: str | None = None):
        return self.sftp.get(remotepath=remotepath, localpath=localpath)

    def put(self, remotepath: str, localpath: str | None = None):
        return self.sftp.put(remotepath=remotepath, localpath=localpath)

    def ls(
        self, remotepath: str, with_attr: bool = False
    ) -> list[str] | paramiko.SFTPAttributes:
        if with_attr:
            return self.sftp.listdir_attr(path=remotepath)
        else:
            return self.sftp.listdir(path=remotepath)

    # def rm()


if __name__ == "__main__":
    from rich import print

    credentials = {
        "host": "s-94284420a37341589.server.transfer.eu-west-1.amazonaws.com",
        "username": "hubgrade-hongkong",
        "password": "0.zKiW?F:-.7kdJD",
    }

    files = [
        # uplaoded to asia_data_raw
        "01_dummy.csv",
        #     "nagpur-ocw-v_20221114065407.csv",
        #     "nagpur-ocw-v_20221114065437.csv",
        #     "nagpur-ocw-v_20221114065445.csv",
        #     "nagpur-ocw-v_20221114065547.csv",
        #     "nagpur-ocw-v_20221114065554.csv",
        #     "nagpur-ocw-v_20221114070826.csv",
        #     "nagpur-ocw-v_20221114070839.csv",
        #     "nagpur-ocw-v_20221114070931.csv",
        #     "nagpur-ocw-v_20221114071013.csv",
    ]

    sftp = SFTP(credentials=credentials)

    with sftp:

        # for f in files:
        # sftp.sftp.remove(path=f"/input/{f}")
        print(sftp.ls("/input"))
        # print(sftp)
        # print(sftp.ls("./input"))
