import logging
from pathlib import Path
import sys
from datetime import datetime, timedelta


def get_logger(logger_name, log_level=None, log_output_folder: str | None = None):

    # clean historical logger_files
    clean_logger_files(log_output_folder=log_output_folder, days_ago=30)

    # logger = logging.getLogger(__name__)
    # logger.setLevel(logging.DEBUG)
    logger = logging.getLogger(logger_name)

    if log_level is None:
        log_level = logging.DEBUG
    logger.setLevel(log_level)
    formatter = logging.Formatter(
        "%(asctime)s - %(levelname)s - %(module)s - %(funcName)s: %(message)s",
        datefmt="%Y-%m-%d %H:%M:%S",
    )

    # logging.basicConfig(
    #     filename="abc.txt",
    #     filemode="a",
    #     format="%(asctime)s,%(msecs)d %(name)s %(levelname)s %(message)s",
    #     datefmt="%H:%M:%S",
    #     level=logging.DEBUG,
    # )
    datestamp = datetime.now().date()
    log_fn = f"log_{datestamp}.txt"

    if log_output_folder is None:
        log_output_folder = "logs"

    log_output_folder = Path(log_output_folder)
    log_output_folder.mkdir(exist_ok=True)
    log_fp = log_output_folder / log_fn
    log_fp = Path(log_output_folder) / log_fn

    stdout_handler = logging.StreamHandler(sys.stdout)
    stdout_handler.setFormatter(formatter)

    file_handler = logging.FileHandler(log_fp)
    file_handler.setFormatter(formatter)

    logger.addHandler(stdout_handler)
    logger.addHandler(file_handler)
    return logger


def clean_logger_files(log_output_folder, days_ago: int):
    log_files = Path("logs").glob("*.txt")

    a_month_ago = datetime.now() - timedelta(days=days_ago)

    for f in log_files:
        creation_time = datetime.fromtimestamp((f.stat()).st_ctime)

        if creation_time < a_month_ago:

            f.unlink()


# clean_logger_files(days_ago=-1)
