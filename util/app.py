import json
import copy
from collections import defaultdict
import logging
from pathlib import Path
from datetime import datetime, timedelta

import pandas as pd

from util.mapping import mapping
from util import SFTP, get_logger

logger = get_logger(logger_name=__name__, log_level=logging.DEBUG)


class NagpurUploader:
    def __init__(self, config: dict[str, str]) -> None:

        self.config = config
        self.dry_run = config.get("dry_run", False)
        self.files_processed = []
        self.runtime_info = defaultdict(list)
        # clear up temp dir for files operaiton
        # self._folder = Path(config.get("success_raw_folder", "success_raw"))
        self.output_folder = Path(config.get("output_folder", "output"))

        self.output_folder.mkdir(exist_ok=True)
        # [f.unlink() for f in dest_folder.rglob("*")]

        # get all folders in root dir
        rf = Path(config["src_folder"])
        self.folders = [p for p in rf.glob("*") if not p.is_file()]
        logger.info(
            f"list of subfolders under {rf} - {[folder.name for folder in self.folders]}"
        )
        # self.folders = [Path("dev/root/157791")]

    def get_yesterday_files(self, folder: Path):
        output = []
        yesterday = (datetime.now() - timedelta(days=1)).date()
        for f in folder.glob("*.csv"):
            last_modified_time = (f.stat()).st_mtime
            last_modified_date = (datetime.fromtimestamp(last_modified_time)).date()
            if last_modified_date == yesterday:
                output.append(f)
        return output

    def raw_to_sftp_format(self):
        """for each folder, load all csv into one df, then tranform to sftp
        format"""

        output_dfs = []
        for folder in self.folders:
            logger.debug(f"working on folder - {folder}")
            sr_number = folder.name
            curr_mapping = mapping.get(sr_number, None)
            # stop processing for folder if it isn't in mapping
            if curr_mapping is None:
                logger.warning(f"sr_number not found in mapping: {sr_number}")
                continue
            logger.debug(f"mapping fetched - {curr_mapping}")

            # combine each csv file within folder to one dataframe
            dfs = []
            # files = [f for f in folder.glob("*.csv")]
            files = self.get_yesterday_files(folder=folder)
            if not files:
                logger.debug(f"there is no yesterday files in folder - {folder}")
                continue

            logger.debug(f"raw csv in current folder - {[str(f) for f in files]}")

            for f in files:
                logger.debug(f"working on file - {str(f)}")

                try:
                    df = pd.read_csv(
                        str(f),
                        skiprows=1,
                        encoding="utf-16-le",
                    )

                    dfs.append(df)
                    logger.debug(f"successfully read raw csv as df - {str(f)}")

                except Exception:
                    logger.warning(f"fail to read raw csv as df - {str(f)}")
                    self.runtime_info["err_read_to_df"].append(f)
                    continue

            combined = pd.concat(dfs)

            # transform raw data to desired formatj
            logger.info("beginning raw csv file tranformation")
            try:
                output_format_df = self._df_transform(
                    raw_df=combined, mapping=curr_mapping
                )
                output_dfs.append(output_format_df)
                logger.info("completed raw csv file tranformation")

            except Exception:
                logger.info(
                    f"error in raw csv file tranformation for folder - {folder}"
                )

                self.runtime_info["err_transform"].extend([str(f) for f in files])
                continue
            else:
                # successfully transformed files, for later process
                self.files_processed.extend([str(f) for f in files])
                logger.debug(f"outstanding processed files: {self.files_processed}")
        # write to local
        now_ts = datetime.utcnow().strftime("%Y%m%d%H%M%S")
        output = pd.concat(output_dfs)
        self.outpath = self.output_folder / f"nagpur-ocw-v_{now_ts}.csv"
        output.to_csv(self.outpath, index=False)
        logger.info(f"transformed CSV created in: {self.outpath}")

    def _df_transform(self, raw_df: pd.DataFrame, mapping: dict) -> pd.DataFrame:
        def _raw_col_to_measure_point(x):
            output = copy.deepcopy(mapping)
            if "Flow" in x:
                output.update(mapping["measure"]["flow"])
            elif "Pressure" in x:
                output.update(mapping["measure"]["pressure"])

            elif "Total" in x:
                output.update(mapping["measure"]["flow"])
                output["measure"] += " Total"
                output["unit"] = "m3"
                # output.update(mapping["measure"]["pressure"])

            else:
                raise ValueError("unexpected row value")

            return json.dumps(output)

        relevant_cols = [
            "Date",
            "Time",
            "Average Flow D1a (m³/h)",
            "Average Pressure A1 (Kg/cm²)",
            "Cumulative Total Volume D1a (m³)",
        ]
        # keep only needed col
        df = raw_df[raw_df.columns.intersection(relevant_cols)]

        # measure_point: unpivot multiple columns if it exist
        df = pd.melt(df, id_vars=["Date", "Time"], var_name="measure_point")
        df = df.dropna()
        # measure_point: from mapping, using fn key to look for relevant measure
        # point
        df["measure_point"] = df["measure_point"].apply(func=_raw_col_to_measure_point)

        # measure_time: Date + Time
        df["measure_time"] = pd.to_datetime(
            df["Date"] + " " + df["Time"], format="%m/%d/%Y %H:%M:%S"
        )

        df.drop(columns=["Date", "Time"], inplace=True)

        # measure_value
        df = df.round(decimals=2)
        df.rename({"value": "measure_value"}, inplace=True, axis=1)

        # reorder output
        col_orders = ["measure_time", "measure_point", "measure_value"]
        df = df[col_orders]
        return df

    def clean_up_files(self):

        success_raw_folder = Path(self.config.get("success_raw_folder", "success_raw"))
        failure_folder = Path(self.config.get("failure_folder", "failure"))

        def mv(file_path, dest_folder):
            dest_folder = Path(dest_folder)
            dest_folder.mkdir(exist_ok=True)

            current_path = Path(file_path)
            dest_path = dest_folder / current_path.name

            log_msg = f"moved raw file {current_path.name} from {current_path.parent} to {dest_path.parent}"
            if self.dry_run:
                logger.debug(f"***dry run*** - no file movement locally - {log_msg}")
            else:
                current_path.rename(dest_path)
                logger.debug(log_msg)

        # move raw files to success folder
        success_raw_folder.mkdir(exist_ok=True)
        for f in self.files_processed:
            mv(f, success_raw_folder)

        # move raw files to failure folder
        if err_read_to_df := self.runtime_info.get("err_read_to_df"):
            err_fol = failure_folder / "err_read_to_df"
            err_fol.mkdir(exist_ok=True)
            for f in err_read_to_df:
                mv(f, err_fol)

        if err_transform := self.runtime_info.get("err_transform"):
            err_fol = failure_folder / "err_transform"
            err_fol.mkdir(exist_ok=True)

            for f in err_transform:
                mv(f, err_fol)

    def to_sftp(self):
        credentials = {
            "host": "s-94284420a37341589.server.transfer.eu-west-1.amazonaws.com",
            "username": "hubgrade-hongkong",
            "password": "0.zKiW?F:-.7kdJD",
        }

        sftp = SFTP(credentials=credentials)

        if self.dry_run:
            logger.debug(
                f"***dry run*** - no file uploaded to SFTP server - uploaded sftp file: {self.outpath}"
            )
        else:
            with sftp:
                logger.debug("connected to SFTP server")

                # for f in self.output_folder.glob("nagpur-ocw-v_*.csv"):
                if self.dry_run is False:
                    logger.debug(f"uploading sftp file: {self.outpath}")
                    sftp.put(
                        localpath=str(self.outpath),
                        remotepath="/input/" + self.outpath.name,
                    )
                    logger.debug(f"uploaded sftp file: {self.outpath}")


if __name__ == "__main__":

    # TODO: df.to_csv date format need to be "08/24/2022 00:00:09"

    from rich import print
    from setting import config

    # sts = SiteToSFTP("dev/root")

    nagpur = NagpurUploader(config=config)

    # for f in files:
    #     print(f)
    # nagpur.raw_to_sftp_format()
    # nagpur.to_sftp()
    # nagpur.clean_up_files()

    # print(nagpur.files_processed)
    # print(nagpur.runtime_info)
    # print(df)
    # print(df.columns)
    # df_new = df[
    #     df.columns.intersection(
    #         set(
    #             [
    #                 "Date",
    #                 "Time",
    #                 "Average Flow D1a (m³/h)",
    #                 "Average Pressure A1 (Kg/cm²)",
    #             ]
    #         )
    #     )
    # ]
    # print(df_new)
