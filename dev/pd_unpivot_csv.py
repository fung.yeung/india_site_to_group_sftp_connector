import pandas as pd
from rich import print

f = "/home/fung/Documents/work/project/india_site_to_group_sftp_connector/dev/asset/test.csv"
df = pd.read_csv(f, skiprows=1)
output = pd.melt(df, id_vars=["date", "time"], var_name="measure_point")
output = output.dropna()

print(output)
