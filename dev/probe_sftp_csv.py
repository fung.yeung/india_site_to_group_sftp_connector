from rich import print
from pathlib import Path
import pandas as pd


files = Path(
    "/home/fung/Documents/project/india_site_to_group_sftp_connector/dev/from_site"
).glob("nagpur*.csv")

dfs = []
for f in files:
    dfs.append(pd.read_csv(f))
    out = pd.concat(dfs)

# out.to_csv("./dev/from_site/concat.csv", index=False)

# print(out)
cond = out["measure_point"].str.contains("FLOW")
print(out[cond]["measure_point"])

# print(out)
