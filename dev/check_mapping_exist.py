from mapping import mapping
import pandas as pd
from rich import print

bq = pd.read_csv("dev/asset/bq_site_measure_list.csv")

df_d = {"site": [], "measure": []}
for _, site_d in mapping.items():
    for _, v in site_d.items():
        df_d["site"].append(v["site"])
        df_d["measure"].append(v["measure"])

mysql = pd.DataFrame(df_d)

bq.reset_index(inplace=True, drop=True)
mysql.reset_index(inplace=True, drop=True)

df = pd.merge(bq, mysql, how="outer", suffixes=("", "_y"), indicator=True)
rows_in_df1_not_in_df2 = df[df["_merge"] == "left_only"][bq.columns]
print(rows_in_df1_not_in_df2.to_csv("dev/asset/diff_site_measure.csv", index=False))

# cond1 = df["site"].isin(mysql["site"])
# cond2 = df["measure"].isin(mysql["measure"])
# output = df[cond1 & cond2]
# print(output)
# # print(mysql)

# # cond2 = [df.measure == mysql.measure]
# # print(df_d[cond1 & cond2])
# print(df.index)
