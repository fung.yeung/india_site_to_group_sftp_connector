mapping = {
    "2262": {
        "pressure": {
            "zone": "1 Laxminagar",
            "site": "Khamla",
            "area": "",
            "measure": "CMP HDMA 1",
            "unit": "kg/cm2",
        }
    },
    "1858": {
        "pressure": {
            "zone": "1 Laxminagar",
            "site": "Khamla",
            "area": "",
            "measure": "CMP HDMA 2",
            "unit": "kg/cm2",
        }
    },
    "157800": {
        "flow": {
            "zone": "1 Laxminagar",
            "site": "Khamla",
            "area": "",
            "measure": "HDMA 1 BRANCH 2 FLOW",
            "unit": "m3/hr",
        },
        "pressure": {
            "zone": "1 Laxminagar",
            "site": "Khamla",
            "area": "",
            "measure": "HDMA 1 BRANCH 2 PRESSURE",
            "unit": "kg/cm2",
        },
    },
    "1877": {
        "pressure": {
            "zone": "2 Dharampeth",
            "site": "Tekdiwadi",
            "area": "",
            "measure": "CMP HDMA 1",
            "unit": "kg/cm2",
        }
    },
    "203676": {
        "pressure": {
            "zone": "2 Dharampeth",
            "site": "Tekdiwadi",
            "area": "",
            "measure": "CMP HDMA 2",
            "unit": "kg/cm2",
        }
    },
    "157793": {
        "flow": {
            "zone": "2 Dharampeth",
            "site": "Tekdiwadi",
            "area": "",
            "measure": "BRANCH 1 FLOW",
            "unit": "m3/hr",
        },
        "pressure": {
            "zone": "2 Dharampeth",
            "site": "Tekdiwadi",
            "area": "",
            "measure": "BRANCH 1 PRESSURE",
            "unit": "kg/cm2",
        },
    },
    "157814": {
        "flow": {
            "zone": "2 Dharampeth",
            "site": "Tekdiwadi",
            "area": "",
            "measure": "BRANCH 2 FLOW",
            "unit": "m3/hr",
        },
        "pressure": {
            "zone": "2 Dharampeth",
            "site": "Tekdiwadi",
            "area": "",
            "measure": "BRANCH 2 PRESSURE",
            "unit": "kg/cm2",
        },
    },
    "2312": {
        "pressure": {
            "zone": "1 Laxminagar",
            "site": "Trimurtinagar",
            "area": "",
            "measure": "CMP HDMA 1",
            "unit": "kg/cm2",
        }
    },
    "1859": {
        "pressure": {
            "zone": "1 Laxminagar",
            "site": "Trimurtinagar",
            "area": "",
            "measure": "CMP HDMA 2",
            "unit": "kg/cm2",
        }
    },
    "133915": {
        "flow": {
            "zone": "1 Laxminagar",
            "site": "Trimurtinagar",
            "area": "",
            "measure": "BRANCH 1 FLOW",
            "unit": "m3/hr",
        },
        "pressure": {
            "zone": "1 Laxminagar",
            "site": "Trimurtinagar",
            "area": "",
            "measure": "BRANCH 1 PRESSURE",
            "unit": "kg/cm2",
        },
    },
    "156605": {
        "flow": {
            "zone": "1 Laxminagar",
            "site": "Trimurtinagar",
            "area": "",
            "measure": "BRANCH 2 FLOW",
            "unit": "m3/hr",
        },
        "pressure": {
            "zone": "1 Laxminagar",
            "site": "Trimurtinagar",
            "area": "",
            "measure": "BRANCH 2 PRESSURE",
            "unit": "kg/cm2",
        },
    },
    "2310": {
        "pressure": {
            "zone": "3 HanumanNagar",
            "site": "Omkar Nagar 1",
            "area": "",
            "measure": "CMP HDMA 1",
            "unit": "kg/cm2",
        }
    },
    "2263": {
        "pressure": {
            "zone": "3 HanumanNagar",
            "site": "Omkar Nagar 1",
            "area": "",
            "measure": "CMP HDMA 2",
            "unit": "kg/cm2",
        }
    },
    "1855": {
        "pressure": {
            "zone": "3 HanumanNagar",
            "site": "Omkar Nagar 2",
            "area": "",
            "measure": "CMP HDMA 1",
            "unit": "kg/cm2",
        }
    },
    "139601": {
        "pressure": {
            "zone": "3 HanumanNagar",
            "site": "Omkar Nagar 2",
            "area": "",
            "measure": "CMP HDMA 2",
            "unit": "kg/cm2",
        }
    },
    "1870": {
        "pressure": {
            "zone": "4 Dhantoli",
            "site": "Wanjari Nagar 1",
            "area": "",
            "measure": "CMP HDMA 1",
            "unit": "kg/cm2",
        }
    },
    "139587": {
        "pressure": {
            "zone": "4 Dhantoli",
            "site": "Wanjari Nagar 1",
            "area": "",
            "measure": "CMP HDMA 2",
            "unit": "kg/cm2",
        }
    },
    "139599": {
        "pressure": {
            "zone": "3 HanumanNagar",
            "site": "NalandaNagar",
            "area": "",
            "measure": "CMP HDMA 1",
            "unit": "kg/cm2",
        }
    },
    "203684": {
        "pressure": {
            "zone": "3 HanumanNagar",
            "site": "NalandaNagar",
            "area": "",
            "measure": "CMP HDMA 2",
            "unit": "kg/cm2",
        }
    },
    "200401": {
        "pressure": {
            "zone": "3 HanumanNagar",
            "site": "Shrinagar",
            "area": "",
            "measure": "CMP HDMA 1",
            "unit": "kg/cm2",
        }
    },
    "2299": {
        "pressure": {
            "zone": "3 HanumanNagar",
            "site": "Shrinagar",
            "area": "",
            "measure": "CMP HDMA 2",
            "unit": "kg/cm2",
        }
    },
    "200407": {
        "pressure": {
            "zone": "3 HanumanNagar",
            "site": "Shrinagar",
            "area": "",
            "measure": "CMP HDMA 3",
            "unit": "kg/cm2",
        }
    },
    "1868": {
        "pressure": {
            "zone": "3 HanumanNagar",
            "site": "Chichbhuvan",
            "area": "",
            "measure": "CMP HDMA 1",
            "unit": "kg/cm2",
        }
    },
    "2085": {
        "pressure": {
            "zone": "3 HanumanNagar",
            "site": "Chichbhuvan",
            "area": "",
            "measure": "CMP HDMA 2",
            "unit": "kg/cm2",
        }
    },
    "2317": {
        "pressure": {
            "zone": "3 HanumanNagar",
            "site": "Chichbhuvan",
            "area": "",
            "measure": "CMP TEST 1",
            "unit": "kg/cm2",
        }
    },
    "1854": {
        "pressure": {
            "zone": "3 HanumanNagar",
            "site": "Chichbhuvan NIT",
            "area": "",
            "measure": "CMP HDMA 1",
            "unit": "kg/cm2",
        }
    },
    "1863": {
        "pressure": {
            "zone": "4 Dhantoli",
            "site": "Wanjari Nagar 2",
            "area": "",
            "measure": "CMP HDMA 1",
            "unit": "kg/cm2",
        }
    },
    "1867": {
        "pressure": {
            "zone": "4 Dhantoli",
            "site": "Wanjari Nagar 2",
            "area": "",
            "measure": "CMP HDMA 2",
            "unit": "kg/cm2",
        }
    },
    "1871": {
        "pressure": {
            "zone": "4 Dhantoli",
            "site": "Reshimbagh",
            "area": "",
            "measure": "CMP HDMA 1",
            "unit": "kg/cm2",
        }
    },
    "1874": {
        "pressure": {
            "zone": "4 Dhantoli",
            "site": "Reshimbagh",
            "area": "",
            "measure": "CMP HDMA 2",
            "unit": "kg/cm2",
        }
    },
    "139577": {
        "pressure": {
            "zone": "4 Dhantoli",
            "site": "Reshimbagh",
            "area": "",
            "measure": "CMP HDMA 3",
            "unit": "kg/cm2",
        }
    },
    "139588": {
        "pressure": {
            "zone": "2 Dharampeth",
            "site": "Dhantoli",
            "area": "",
            "measure": "CMP HDMA 1",
            "unit": "kg/cm2",
        }
    },
    "139606": {
        "pressure": {
            "zone": "2 Dharampeth",
            "site": "Dabha",
            "area": "",
            "measure": "CMP HDMA 1",
            "unit": "kg/cm2",
        }
    },
    "139584": {
        "pressure": {
            "zone": "2 Dharampeth",
            "site": "Demo Zone Futala Line",
            "area": "",
            "measure": "CMP HDMA 2",
            "unit": "kg/cm2",
        }
    },
    "139590": {
        "pressure": {
            "zone": "2 Dharampeth",
            "site": "Demo Zone Civil Line DT",
            "area": "",
            "measure": "CMP HDMA 1",
            "unit": "kg/cm2",
        }
    },
    "2275": {
        "pressure": {
            "zone": "2 Dharampeth",
            "site": "Demo Zone Rifle Line",
            "area": "",
            "measure": "CMP HDMA 1",
            "unit": "kg/cm2",
        }
    },
    "139608": {
        "pressure": {
            "zone": "2 Dharampeth",
            "site": "Demo Zone Ramnagar ESR",
            "area": "",
            "measure": "CMP HDMA 1",
            "unit": "kg/cm2",
        }
    },
    "2318": {
        "pressure": {
            "zone": "2 Dharampeth",
            "site": "Demo Zone Ramnagar GSR",
            "area": "",
            "measure": "CMP HDMA 1",
            "unit": "kg/cm2",
        }
    },
    "139571": {
        "pressure": {
            "zone": "2 Dharampeth",
            "site": "GH Buldi",
            "area": "",
            "measure": "CMP HDMA 1",
            "unit": "kg/cm2",
        }
    },
    "1862": {
        "pressure": {
            "zone": "2 Dharampeth",
            "site": "GH Buldi",
            "area": "",
            "measure": "CMP HDMA 2",
            "unit": "kg/cm2",
        }
    },
    "2315": {
        "pressure": {
            "zone": "1 Laxminagar",
            "site": "GayatriNagar",
            "area": "",
            "measure": "CMP HDMA 1",
            "unit": "kg/cm2",
        }
    },
    "139581": {
        "pressure": {
            "zone": "1 Laxminagar",
            "site": "GayatriNagar",
            "area": "",
            "measure": "CMP HDMA 2",
            "unit": "kg/cm2",
        }
    },
    "1878": {
        "pressure": {
            "zone": "1 Laxminagar",
            "site": "GayatriNagar",
            "area": "",
            "measure": "CMP HDMA 3",
            "unit": "kg/cm2",
        }
    },
    "1861": {
        "pressure": {
            "zone": "1 Laxminagar",
            "site": "PratapNagar",
            "area": "",
            "measure": "CMP HDMA 1",
            "unit": "kg/cm2",
        }
    },
    "139595": {
        "pressure": {
            "zone": "1 Laxminagar",
            "site": "PratapNagar",
            "area": "",
            "measure": "CMP HDMA 2",
            "unit": "kg/cm2",
        }
    },
    "200406": {
        "pressure": {
            "zone": "1 Laxminagar",
            "site": "TakliSeem",
            "area": "",
            "measure": "CMP HDMA 1",
            "unit": "kg/cm2",
        }
    },
    "2086": {
        "pressure": {
            "zone": "1 Laxminagar",
            "site": "TakliSeem",
            "area": "",
            "measure": "CMP HDMA 2",
            "unit": "kg/cm2",
        }
    },
    "2301": {
        "pressure": {
            "zone": "1 Laxminagar",
            "site": "Jaitala",
            "area": "",
            "measure": "CMP HDMA 1",
            "unit": "kg/cm2",
        }
    },
    "2307": {
        "pressure": {
            "zone": "1 Laxminagar",
            "site": "Jaitala",
            "area": "",
            "measure": "CMP HDMA 3",
            "unit": "kg/cm2",
        }
    },
    "139578": {
        "pressure": {
            "zone": "1 Laxminagar",
            "site": "Laxminagar 2",
            "area": "",
            "measure": "CMP HDMA 1",
            "unit": "kg/cm2",
        }
    },
    "200413": {
        "pressure": {
            "zone": "1 Laxminagar",
            "site": "Laxminagar 2",
            "area": "",
            "measure": "CMP HDMA 2",
            "unit": "kg/cm2",
        }
    },
    "1857": {
        "pressure": {
            "zone": "1 Laxminagar",
            "site": "Laxminagar 1",
            "area": "",
            "measure": "CMP HDMA 1",
            "unit": "kg/cm2",
        }
    },
    "2265": {
        "pressure": {
            "zone": "1 Laxminagar",
            "site": "Laxminagar 1",
            "area": "",
            "measure": "CMP HDMA 2",
            "unit": "kg/cm2",
        }
    },
    "139572": {
        "pressure": {
            "zone": "10 Mangalwari",
            "site": "GH Sadar",
            "area": "",
            "measure": "CMP HDMA 1",
            "unit": "kg/cm2",
        }
    },
    "200404": {
        "pressure": {
            "zone": "10 Mangalwari",
            "site": "GH Sadar",
            "area": "",
            "measure": "CMP HDMA 2",
            "unit": "kg/cm2",
        }
    },
    "139602": {
        "pressure": {
            "zone": "10 Mangalwari",
            "site": "GH Sadar",
            "area": "",
            "measure": "CMP HDMA 3",
            "unit": "kg/cm2",
        }
    },
    "139574": {
        "pressure": {
            "zone": "10 Mangalwari",
            "site": "GH Rajnagar",
            "area": "",
            "measure": "CMP HDMA 1",
            "unit": "kg/cm2",
        }
    },
    "200402": {
        "pressure": {
            "zone": "10 Mangalwari",
            "site": "GH Rajnagar",
            "area": "",
            "measure": "CMP HDMA 2",
            "unit": "kg/cm2",
        }
    },
    "200408": {
        "pressure": {
            "zone": "10 Mangalwari",
            "site": "GH Rajnagar",
            "area": "",
            "measure": "CMP HDMA 3",
            "unit": "kg/cm2",
        }
    },
    "2285": {
        "pressure": {
            "zone": "10 Mangalwari",
            "site": "Ramdeobaba GSR",
            "area": "",
            "measure": "CMP HDMA 1",
            "unit": "kg/cm2",
        }
    },
    "1879": {
        "pressure": {
            "zone": "10 Mangalwari",
            "site": "Ramdeobaba GSR",
            "area": "",
            "measure": "CMP HDMA 2",
            "unit": "kg/cm2",
        }
    },
    "203677": {
        "pressure": {
            "zone": "10 Mangalwari",
            "site": "GodhaniGSR",
            "area": "",
            "measure": "CMP HDMA 1",
            "unit": "kg/cm2",
        }
    },
    "2297": {
        "pressure": {
            "zone": "10 Mangalwari",
            "site": "GodhaniGSR",
            "area": "",
            "measure": "CMP HDMA 2",
            "unit": "kg/cm2",
        }
    },
    "200403": {
        "pressure": {
            "zone": "6 Gandhibagh",
            "site": "Killamahal",
            "area": "",
            "measure": "CMP HDMA 1",
            "unit": "kg/cm2",
        }
    },
    "139575": {
        "pressure": {
            "zone": "6 Gandhibagh",
            "site": "Killamahal",
            "area": "",
            "measure": "CMP HDMA 2",
            "unit": "kg/cm2",
        }
    },
    "139596": {
        "pressure": {
            "zone": "6 Gandhibagh",
            "site": "Buldi Fort 1",
            "area": "",
            "measure": "CMP HDMA 1",
            "unit": "kg/cm2",
        }
    },
    "139576": {
        "pressure": {
            "zone": "6 Gandhibagh",
            "site": "Buldi Fort 2",
            "area": "",
            "measure": "CMP HDMA 1",
            "unit": "kg/cm2",
        }
    },
    "2287": {
        "pressure": {
            "zone": "6 Gandhibagh",
            "site": "Buldi Fort 2",
            "area": "",
            "measure": "CMP HDMA 2",
            "unit": "kg/cm2",
        }
    },
    "139605": {
        "pressure": {
            "zone": "5 NehruNagar",
            "site": "Nadanvan Exst",
            "area": "",
            "measure": "CMP HDMA 2",
            "unit": "kg/cm2",
        }
    },
    "1875": {
        "pressure": {
            "zone": "5 NehruNagar",
            "site": "Dighori",
            "area": "",
            "measure": "CMP HDMA 2",
            "unit": "kg/cm2",
        }
    },
    "1876": {
        "pressure": {
            "zone": "5 NehruNagar",
            "site": "Dighori",
            "area": "",
            "measure": "CMP HDMA 3",
            "unit": "kg/cm2",
        }
    },
    "2292": {
        "pressure": {
            "zone": "5 NehruNagar",
            "site": "Kharbi",
            "area": "",
            "measure": "CMP HDMA 1",
            "unit": "kg/cm2",
        }
    },
    "2296": {
        "pressure": {
            "zone": "5 NehruNagar",
            "site": "Kharbi",
            "area": "",
            "measure": "CMP HDMA 2",
            "unit": "kg/cm2",
        }
    },
    "2303": {
        "pressure": {
            "zone": "5 NehruNagar",
            "site": "Sakkardara 1 and 2",
            "area": "",
            "measure": "CMP HDMA 1",
            "unit": "kg/cm2",
        }
    },
    "2309": {
        "pressure": {
            "zone": "5 NehruNagar",
            "site": "Sakkardara 1 and 2",
            "area": "",
            "measure": "CMP HDMA 2",
            "unit": "kg/cm2",
        }
    },
    "2302": {
        "pressure": {
            "zone": "5 NehruNagar",
            "site": "Sakkardara 1 and 2",
            "area": "",
            "measure": "CMP HDMA 3",
            "unit": "kg/cm2",
        }
    },
    "2304": {
        "pressure": {
            "zone": "5 NehruNagar",
            "site": "Sakkardara 1 and 2",
            "area": "",
            "measure": "CMP HDMA 4",
            "unit": "kg/cm2",
        }
    },
    "139585": {
        "pressure": {
            "zone": "9 AshiNagar",
            "site": "Nara",
            "area": "",
            "measure": "CMP HDMA 1",
            "unit": "kg/cm2",
        }
    },
    "1860": {
        "pressure": {
            "zone": "9 AshiNagar",
            "site": "Nara",
            "area": "",
            "measure": "CMP HDMA 2",
            "unit": "kg/cm2",
        }
    },
    "2279": {
        "pressure": {
            "zone": "9 AshiNagar",
            "site": "Bezanbagh",
            "area": "",
            "measure": "CMP HDMA 1",
            "unit": "kg/cm2",
        }
    },
    "2271": {
        "pressure": {
            "zone": "9 AshiNagar",
            "site": "Bezanbagh",
            "area": "",
            "measure": "CMP HDMA 2",
            "unit": "kg/cm2",
        }
    },
    "2288": {
        "pressure": {
            "zone": "9 AshiNagar",
            "site": "Jaripatka 1",
            "area": "",
            "measure": "CMP HDMA 1",
            "unit": "kg/cm2",
        }
    },
    "2305": {
        "pressure": {
            "zone": "9 AshiNagar",
            "site": "Jaripatka 2",
            "area": "",
            "measure": "CMP HDMA 2",
            "unit": "kg/cm2",
        }
    },
    "139583": {
        "pressure": {
            "zone": "8 Lakadganj",
            "site": "Kalamana",
            "area": "",
            "measure": "CMP HDMA 1",
            "unit": "kg/cm2",
        }
    },
    "139582": {
        "pressure": {
            "zone": "8 Lakadganj",
            "site": "MiniMataNagar",
            "area": "",
            "measure": "CMP HDMA 1",
            "unit": "kg/cm2",
        }
    },
    "1856": {
        "pressure": {
            "zone": "8 Lakadganj",
            "site": "Bharatwadi",
            "area": "",
            "measure": "CMP HDMA 1",
            "unit": "kg/cm2",
        }
    },
    "200411": {
        "pressure": {
            "zone": "8 Lakadganj",
            "site": "Bharatwadi",
            "area": "",
            "measure": "CMP HDMA 2",
            "unit": "kg/cm2",
        }
    },
    "2270": {
        "pressure": {
            "zone": "8 Lakadganj",
            "site": "Babulban",
            "area": "",
            "measure": "CMP HDMA 1",
            "unit": "kg/cm2",
        }
    },
    "2274": {
        "pressure": {
            "zone": "8 Lakadganj",
            "site": "Babulban",
            "area": "",
            "measure": "CMP HDMA 2",
            "unit": "kg/cm2",
        }
    },
    "2295": {
        "pressure": {
            "zone": "8 Lakadganj",
            "site": "Subhannagar",
            "area": "",
            "measure": "CMP HDMA 1",
            "unit": "kg/cm2",
        }
    },
    "2300": {
        "pressure": {
            "zone": "8 Lakadganj",
            "site": "Subhannagar",
            "area": "",
            "measure": "CMP HDMA 2",
            "unit": "kg/cm2",
        }
    },
    "2289": {
        "pressure": {
            "zone": "8 Lakadganj",
            "site": "Lakadganj 1",
            "area": "",
            "measure": "CMP HDMA 1",
            "unit": "kg/cm2",
        }
    },
    "2314": {
        "pressure": {
            "zone": "8 Lakadganj",
            "site": "Lakadganj 2",
            "area": "",
            "measure": "CMP HDMA 1",
            "unit": "kg/cm2",
        }
    },
    "2313": {
        "pressure": {
            "zone": "8 Lakadganj",
            "site": "Lakadganj 2",
            "area": "",
            "measure": "CMP HDMA 2",
            "unit": "kg/cm2",
        }
    },
    "2319": {
        "pressure": {
            "zone": "8 Lakadganj",
            "site": "Pardi 1",
            "area": "",
            "measure": "CMP HDMA 1",
            "unit": "kg/cm2",
        }
    },
    "2293": {
        "pressure": {
            "zone": "8 Lakadganj",
            "site": "Pardi 1",
            "area": "",
            "measure": "CMP HDMA 2",
            "unit": "kg/cm2",
        }
    },
    "2294": {
        "pressure": {
            "zone": "8 Lakadganj",
            "site": "Pardi 1",
            "area": "",
            "measure": "CMP HDMA 3",
            "unit": "kg/cm2",
        }
    },
    "203686": {
        "pressure": {
            "zone": "8 Lakadganj",
            "site": "Pardi 2",
            "area": "",
            "measure": "CMP HDMA 1",
            "unit": "kg/cm2",
        }
    },
    "200405": {
        "pressure": {
            "zone": "8 Lakadganj",
            "site": "Pardi 2",
            "area": "",
            "measure": "CMP HDMA 2",
            "unit": "kg/cm2",
        }
    },
    "200409": {
        "pressure": {
            "zone": "8 Lakadganj",
            "site": "Pardi 2",
            "area": "",
            "measure": "CMP HDMA 3",
            "unit": "kg/cm2",
        }
    },
    "203687": {
        "pressure": {
            "zone": "7 Satranjipura",
            "site": "Shantinagar",
            "area": "",
            "measure": "CMP HDMA 1",
            "unit": "kg/cm2",
        }
    },
    "2298": {
        "pressure": {
            "zone": "7 Satranjipura",
            "site": "Shantinagar",
            "area": "",
            "measure": "CMP HDMA 2",
            "unit": "kg/cm2",
        }
    },
    "2308": {
        "pressure": {
            "zone": "7 Satranjipura",
            "site": "Boriyapura ESR",
            "area": "",
            "measure": "CMP HDMA 2",
            "unit": "kg/cm2",
        }
    },
    "139609": {
        "pressure": {
            "zone": "7 Satranjipura",
            "site": "Boriyapura 600mm Feeder",
            "area": "",
            "measure": "CMP HDMA 1",
            "unit": "kg/cm2",
        }
    },
    "2290": {
        "pressure": {
            "zone": "7 Satranjipura",
            "site": "Boriyapura 600mm Feeder",
            "area": "",
            "measure": "CMP HDMA 2",
            "unit": "kg/cm2",
        }
    },
    "2306": {
        "pressure": {
            "zone": "7 Satranjipura",
            "site": "Boriyapura 600mm Feeder",
            "area": "",
            "measure": "CMP HDMA 3",
            "unit": "kg/cm2",
        }
    },
    "139579": {
        "pressure": {
            "zone": "7 Satranjipura",
            "site": "Bastarwadi 1 Part A",
            "area": "",
            "measure": "CMP HDMA 1",
            "unit": "kg/cm2",
        }
    },
    "1872": {
        "pressure": {
            "zone": "7 Satranjipura",
            "site": "Bastarwadi 1 Part A",
            "area": "",
            "measure": "CMP HDMA 2",
            "unit": "kg/cm2",
        }
    },
    "1869": {
        "pressure": {
            "zone": "7 Satranjipura",
            "site": "Bastarwadi 1 Part A",
            "area": "",
            "measure": "CMP HDMA 3",
            "unit": "kg/cm2",
        }
    },
    "2286": {
        "pressure": {
            "zone": "7 Satranjipura",
            "site": "Bastarwadi 1 Part B",
            "area": "",
            "measure": "CMP HDMA 1",
            "unit": "kg/cm2",
        }
    },
    "2283": {
        "pressure": {
            "zone": "7 Satranjipura",
            "site": "Bastarwadi 1 Part B",
            "area": "",
            "measure": "CMP HDMA 2",
            "unit": "kg/cm2",
        }
    },
    "139580": {
        "pressure": {
            "zone": "7 Satranjipura",
            "site": "Bastarwadi 1 Part B",
            "area": "",
            "measure": "CMP HDMA 3",
            "unit": "kg/cm2",
        }
    },
    "2291": {
        "pressure": {
            "zone": "7 Satranjipura",
            "site": "Bastarwadi 1 Part B",
            "area": "",
            "measure": "CMP HDMA 4",
            "unit": "kg/cm2",
        }
    },
    "2284": {
        "pressure": {
            "zone": "7 Satranjipura",
            "site": "Bastarwadi 2",
            "area": "",
            "measure": "CMP HDMA 1",
            "unit": "kg/cm2",
        }
    },
    "200412": {
        "pressure": {
            "zone": "7 Satranjipura",
            "site": "Bastarwadi 2",
            "area": "",
            "measure": "CMP HDMA 2",
            "unit": "kg/cm2",
        }
    },
    "2258": {
        "pressure": {
            "zone": "5 NehruNagar",
            "site": "Nandanvan 2",
            "area": "",
            "measure": "CMP HDMA 1",
            "unit": "kg/cm2",
        }
    },
    "2264": {
        "pressure": {
            "zone": "5 NehruNagar",
            "site": "Nandanvan 2",
            "area": "",
            "measure": "CMP HDMA 2",
            "unit": "kg/cm2",
        }
    },
    "2094": {
        "pressure": {
            "zone": "5 NehruNagar",
            "site": "Nandanvan 2",
            "area": "",
            "measure": "CMP HDMA 3",
            "unit": "kg/cm2",
        }
    },
    "139573": {
        "pressure": {
            "zone": "5 NehruNagar",
            "site": "Nandanvan Exst",
            "area": "",
            "measure": "CMP HDMA 1",
            "unit": "kg/cm2",
        }
    },
}
